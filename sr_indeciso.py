#Funcao busca(lista, ini, fim, dinheiro, modo)
#modo 0 -> posicao da corda do primeiro dinheiro maior ou igual a x
#modo 1 -> posicao da corda do ultimo dinheiro menor ou igual a x
def busca(lista, ini, fim, dinheiro, modo):
    pivo = int((ini + fim + 1) / 2)
    if modo == 0:
        if lista[pivo] == dinheiro:
            if ((pivo - 1) < ini):
                return pivo
            else:
                if (lista[pivo - 1] != dinheiro):
                    return pivo
                else:
                    pivo = busca(lista, ini, (pivo - 1), dinheiro, 0)

        elif lista[pivo] > dinheiro:
            if ((pivo - 1) < ini):
                return pivo
            else:
                if (lista[pivo - 1] < dinheiro):
                    return pivo
                else:
                    pivo = busca(lista, ini, (pivo-1), dinheiro, 0)

        elif lista[pivo] < dinheiro:
            if ((pivo + 1) > fim):
                return pivo
            else:
                if (lista[pivo + 1] >= dinheiro):
                    return (pivo + 1)
                else:
                    pivo = busca(lista, (pivo + 1), fim, dinheiro, 0)
        return pivo

    elif modo == 1:
        if lista[pivo] == dinheiro:
            if ((pivo + 1) > fim):
                return pivo
            else:
                if(lista[pivo + 1] != dinheiro):
                    return pivo
                else:
                    pivo = busca(lista, (pivo + 1), fim, dinheiro, 1)
        elif lista[pivo] > dinheiro:
            if ((pivo - 1) < ini):
                return pivo
            else:
                if (lista[pivo - 1] <= dinheiro):
                    return (pivo - 1)
                else:
                    pivo = busca(lista, ini, (pivo - 1), dinheiro, 1)
        elif lista[pivo] < dinheiro:
            if ((pivo + 1) > fim):
                return pivo
            else:
                if (lista[pivo + 1] > dinheiro):
                    return pivo
                else:
                    pivo = busca(lista, (pivo + 1), fim, dinheiro, 1)
        return pivo
##################################################################
quant_din = int(input())
#lista_din = [0] * quant_din
lista_din = []
'''
for i in range(0, quant_din):
    h = int(input())

    lista.append(h)
'''
lista_din = list(map(int,input().strip().split()))[:quant_din]
#print(lista_din)
quant_req = int(input())
lista_req = [0] * quant_req

for j in range(quant_req):
    lista_req[j] = int(input())
ini = 0
fim = (len(lista_din) - 1)
for h in range(quant_req):
    aux1 = busca(lista_din, ini, fim, lista_req[h], 0)
    aux2 = busca(lista_din, ini, fim, lista_req[h], 1)

    print(aux1, aux2)
