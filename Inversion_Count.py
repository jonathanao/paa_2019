def quick_sort(vetor, ini, fim):
    i = ini
    j = fim-1
    pivo = vetor[int((ini + fim) / 2)]
    while i <= j:
        while(vetor[i] < pivo) and (i < fim):
            i += 1
        while(vetor[j] > pivo) and (j > ini):
            j -= 1
        if i <= j:
            aux = vetor[i]
            vetor[i] = vetor[j]
            vetor[j] = aux
            i += 1
            j -= 1
    if j > ini:
        quick_sort(vetor, ini, j+1)
    if i < fim:
        quick_sort(vetor, i, fim)


vet = [10, 9, 8, 7, 65, 1, 1, 3, 9, -3, 1000, 109]
a = 0
b = int(len(vet))
quick_sort(vet, a, b)

print(vet)


'''
num_casos = int(input())
entrada_1 = input()
for i in range(num_casos):
    cont = 0
    tam_vetor = int(input())
    vetor = [0]*tam_vetor
    for a in range(tam_vetor):
        vetor[a] = int(input())
    for j in range(tam_vetor):
        h = j+1
        for b in range(h, tam_vetor, 1):
            if vetor[j] > vetor[b]:
                cont += 1
    print(cont)
    if i < num_casos-1:
        entrada_2 = input()
'''
